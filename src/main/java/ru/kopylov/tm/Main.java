package ru.kopylov.tm;

import ru.kopylov.tm.command.ConsoleReader;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * @author Mikhail Kopylov
 * task/project manager
 */

public class Main {
    public static void main(String[] args) {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String inputString = null;
        ConsoleReader consoleReader = new ConsoleReader();

        System.out.println("*** WELCOME TO TASK MANAGER ***");
        do {
            try {
                inputString = reader.readLine();
                consoleReader.readCommand(inputString);
            } catch (IOException e) {
                e.printStackTrace();
            }
        } while (!"exit".equals(inputString));
    }
}
